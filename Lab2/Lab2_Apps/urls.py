from django.conf.urls import include, url
from django.contrib import admin
from django.shortcuts import render
from .views import index

urlpatterns = [
   url(r'^$', index, name='index'),
]