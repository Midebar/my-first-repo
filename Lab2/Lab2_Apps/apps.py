from django.apps import AppConfig


class Lab2AppsConfig(AppConfig):
    name = 'Lab2_Apps'
